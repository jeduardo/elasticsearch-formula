describe package('elasticsearch') do
  it { should be_installed }
  its('version') { should cmp >= '5' }
end

describe file('/etc/elasticsearch/jvm.options') do
  its('type') { should cmp 'file' }
  it { should be_file }
  it { should_not be_directory }
end
